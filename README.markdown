# ltop
[![pipeline status](https://gitlab.com/cyberious/ltop/badges/master/pipeline.svg)](https://gitlab.com/cyberious/ltop/commits/master)

---

1. [What is LTop?](#what-is-ltop)
1. [How to use it?](#how-to-use-it?)
    * [Binary](#binary)
    * [Docker](#docker)
1. [Parameters](#parameters)
1. [Limitations](#limitations)


## What is LTop?
LTop was inspired by the Linux command `top` which will sort and show the current running processes. The `L` stands for
Logs, specifically Apache Logs, it will show the user how many requests are in a particular category, i.e. `50x` errors will
show as `Server Errors` while `40x` errors will show as `Client Errors`, status below that are seen as OK status.  Additionally
we section out to the top level part of the route i.e. `/users/create` would be a section `/users` this is particularly useful
when running several services through an API gateway such as [Traefik](https://traefik.io) or [Kong](https://konghq.com/kong-community-edition/)
and parsing there logs for traffic patterns.
![LTOP Screenshot](https://gitlab.com/cyberious/ltop/raw/master/_docs/images/screenshot.png)
## How to use it?
There are two ways to use the service, one is downloading the binary appropriate to your system.

### Binary
Once you have downloaded the binary for your distro (currently only Mac and Linux are supported).
```bash
ltop -source /var/log/httpd/access.log
```
At this point you should have a UI in front of you

### Docker
The other is to run the container and mount the logs as a volume to your container like so.
Please take note that due to the UI being rendered it does require TTY and must be run with interactive as well, so ensure you are
passing the `-t` and `-i` flags

```bash
docker run -ti -v `pwd`/access.log:/var/log/access.log cyberious/ltop
```

## Parameters
All integers must be a positive integer value, i.e. >= 1
* `source` string
    	Source file to parse for changes (default "/var/log/access.log")
* `interval`: int
    	Seconds between reporting (default 10)  	
* `req-per-sec`: int
    	Requests per second threshold (default 10)
* `threshold-timer` int
    	Threshold timer defaults to 120 (default 120)

## Limitations
Currently there are issues I wish to fix but will take more work.

* [ ] UI lines up differently with a Mac than Linux, particularly the bar graph
* [ ] Historic data seems to fall off the graph view on Mac
* [ ] When loading we only take logs post startup
* [ ] If a logfile is truncated and we will not detect this change
* [ ] If we are unable to parse an entry we simply discard, we should handle this better
* [ ] Log entries are parsed in the interval rather than the second they are added, should enhance
  to have them parsed as they come in with go routines rather than once the sleep runs out.
