package main

import "testing"

func TestCreateRowData(t *testing.T) {
	var data [][]string

	sections := []requestSection{
		{Section: "/", totalCount: 1, okStatusCount: 1},
		{Section: "/report", totalCount: 2, okStatusCount: 1, serverErrCount: 1},
	}

	data = createRowData(sections)

	firstRow := data[1]
	if firstRow[0] != "/" {
		t.Errorf("Expected to have / but got %s\n", firstRow)
	}
	if firstRow[1] != "1" {
		t.Errorf("Expected to get 1 but got %s\n", firstRow[1])
	}
	reportRow := data[2]
	if reportRow[0] != "/report" {
		t.Errorf("Expected to have /report but got %s\n", reportRow)
	}
	if reportRow[1] != "1" {
		t.Errorf("Expected to get 1 but got %s\n", reportRow[1])
	}
}

func TestCreateRow(t *testing.T) {
	req := requestSection{Section: "/", totalCount: 1, okStatusCount: 1}
	row := createRow(req)
	if row[0] != "/" {
		t.Errorf("Expected / but received %s\n", row)
	}
}
