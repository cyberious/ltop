package main

import "os"

type ltopConfig struct {
	source        string
	interval      int
	reqPerSec     int
	thresholdTime int
	logFile       *os.File
}

func (c *ltopConfig) maxReqPerThreshold() int {
	return c.reqPerSec * c.thresholdTime
}


// odd numbers will cause to be not a complete timebox
func (c *ltopConfig) thresholdDataSize() int {
	return c.thresholdTime / c.interval
}
