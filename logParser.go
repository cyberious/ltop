package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"

	"github.com/Songmu/axslogparser"
)

type apacheLogEntry struct {
	Section string
	*axslogparser.Log
}

type apacheLogEntries []apacheLogEntry

type timeBox struct {
	maxSize          int
	latestTotal      int
	allRequestTotals []int
	okRequestTotals  []int
	clientErrTotals  []int
	serverErrTotals  []int
	awaitingRecovery bool
	msg              string
}

func (tb *timeBox) addLatest(sections []requestSection) {
	var total, ok, clientErr, serverErr int
	for _, section := range sections {
		total += section.totalCount
		ok += section.okStatusCount
		clientErr += section.clientErrCount
		serverErr += section.serverErrCount
	}
	tb.latestTotal = total
	tb.allRequestTotals = appendTotals(tb.allRequestTotals, total, tb.maxSize)
	tb.okRequestTotals = appendTotals(tb.okRequestTotals, ok, tb.maxSize)
	tb.clientErrTotals = appendTotals(tb.clientErrTotals, clientErr, tb.maxSize)
	tb.serverErrTotals = appendTotals(tb.serverErrTotals, serverErr, tb.maxSize)
}

// returns total and if it met the threshold
func (tb *timeBox) thresholdMet(config ltopConfig) (string, bool) {
	var total int
	for _, val := range tb.allRequestTotals {
		total += val
	}

	headerMsg := fmt.Sprintf("Threshold requests %d of %d\n", total, config.maxReqPerThreshold())
	if highTraffic := total >= config.maxReqPerThreshold(); highTraffic != tb.awaitingRecovery {
		switch highTraffic {
		case true:
			tb.msg = fmt.Sprintf("High traffic generated an alert - hits = %d, triggered at %s", total, formattedNowTime())
		case false:
			tb.msg = fmt.Sprintf("Recovered from high traffic at %s", formattedNowTime())
		}

		tb.awaitingRecovery = highTraffic

	}

	return headerMsg + tb.msg, tb.awaitingRecovery
}

func appendTotals(tbd []int, latest int, maxSize int) []int {
	if len(tbd) >= maxSize {
		return append(tbd[1:], latest)
	}
	return append(tbd, latest)
}

type requests struct {
	sections []requestSection
	dataset  *timeBox
}

type requestSection struct {
	Section        string
	totalCount     int
	okStatusCount  int
	clientErrCount int
	serverErrCount int
}

func newApacheLogEntry(line string) apacheLogEntry {
	le, err := axslogparser.Parse(line)
	if err != nil {
		log.Printf("An error occured during parsing %s\n", err)
	}
	splitURI := strings.Split(le.RequestURI, "/")
	ale := apacheLogEntry{}
	ale.Section = "/" + splitURI[1]
	ale.Log = le
	return ale
}

func calcRequestsPerSecond(reqCount, currentInterval int) float64 {
	return float64(reqCount) / float64(currentInterval)
}

func getCurrentThroughputPercentage(reqCount int, desiredReqPerSec int, currentInterval int) float64 {
	return calcRequestsPerSecond(reqCount, currentInterval) / float64(desiredReqPerSec) * 100
}

func createSectionTotalsMap(le apacheLogEntries) map[string]requestSection {
	rCounts := map[string]requestSection{}
	for _, log := range le {
		sec := rCounts[log.Section]
		if sec.Section == "" {
			sec.Section = log.Section
		}
		sec.totalCount++
		switch {
		case log.Status < 400:
			sec.okStatusCount++
		case log.Status < 500:
			sec.clientErrCount++
		case log.Status >= 500:
			sec.serverErrCount++
		}

		rCounts[log.Section] = sec
	}

	return rCounts
}

// newSectionalRequests creates a new requests object and attach the current timebox object
// This should be used rather than creating the request object directly as we sort and create the requestSections
func newSectionalRequests(le apacheLogEntries, tb *timeBox) requests {
	rCounts := createSectionTotalsMap(le)
	var req []requestSection

	for _, val := range rCounts {
		req = append(req, val)
	}
	sort.Slice(req, func(i, j int) bool {
		return req[i].totalCount > req[j].totalCount
	})
	tb.addLatest(req)
	return requests{sections: req, dataset: tb}
}

func newTimeBox(c ltopConfig) *timeBox {
	maxSize := c.thresholdDataSize()
	tb := timeBox{
		maxSize:          maxSize,
		allRequestTotals: make([]int, maxSize),
		okRequestTotals:  make([]int, maxSize),
		clientErrTotals:  make([]int, maxSize),
		serverErrTotals:  make([]int, maxSize),
	}
	return &tb
}

func calcSectionTotals(le apacheLogEntries) ([]requestSection, int) {
	rCounts := createSectionTotalsMap(le)
	var req []requestSection
	for _, val := range rCounts {
		req = append(req, val)
	}
	sort.Slice(req, func(i, j int) bool {
		return req[i].totalCount > req[j].totalCount
	})
	return req, len(le)
}

func processCurrentResults(accessLog io.Reader) apacheLogEntries {
	wg := &sync.WaitGroup{}
	accessFileReader := bufio.NewReader(accessLog)
	var logLines []string
	for {
		line, err := accessFileReader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Println("An error was raised reading the file " + err.Error())
			os.Exit(1)
		}
		logLines = append(logLines, line)
	}

	logChannel := make(chan apacheLogEntry, len(logLines))
	for _, line := range logLines {
		wg.Add(1)
		go parseLogAsync(wg, logChannel, line)
	}

	wg.Wait()
	close(logChannel)
	var logResults apacheLogEntries
	for l := range logChannel {
		logResults = append(logResults, l)
	}
	return logResults
}

func openFile(f string) (*os.File, error) {
	sanitizedPath, err := filepath.Abs(f)
	if err != nil {
		return nil, err
	}

	openFile, err := os.Open(sanitizedPath) //nolint

	return openFile, err
}

func parseLogAsync(wg *sync.WaitGroup, c chan apacheLogEntry, line string) {
	defer wg.Done()
	if line != "" {
		c <- newApacheLogEntry(line)
	}
}
