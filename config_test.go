package main

import "testing"

func TestMaxThreshold(t *testing.T) {
	config = ltopConfig{
		interval:      10,
		reqPerSec:     10,
		thresholdTime: 120,
	}
	if config.maxReqPerThreshold() != 1200 {
		t.Errorf("Expected maxReqPerThreshold to be 1200 but got %d", config.maxReqPerThreshold())
	}
}
