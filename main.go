package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"time"

	ui "github.com/gizak/termui"
)

// * Display stats every 10s about the traffic during those 10s: the sections of the web site with the
// most hits, as well as interesting summary statistics on the traffic as a whole. A Section is defined
// as being what's before the second '/' in the path. For example, the Section for
// "http://my.site.com/pages/create” is "http://my.site.com/pages".
//
// * Make sure a user can keep the app running and monitor the log file continuously
//
// * Whenever total traffic for the past 2 minutes exceeds a certain number on average, add a message saying that
// “High traffic generated an alert - hits = {value}, triggered at {time}”.
// The default threshold should be 10 requests per second and should be overridable.
//
// * Whenever the total traffic drops again below that value on average for the past 2 minutes, print or displays another
// message detailing when the alert recovered.
//
// * Write a test for the alerting logic.
//  Explain how you’d improve on this application design.
//  If you have access to a linux docker environment, we'd love to be able to docker build and run your project! If you don't though, don't sweat it. As an example:

var (
	config ltopConfig
)

func init() {
	source := flag.String("source", "/var/log/access.log", "Source file to parse for changes")
	interval := flag.Int("interval", 10, "Seconds between reporting")
	reqPerSec := flag.Int("req-per-sec", 10, "Requests per second threshold")
	thresholdTimer := flag.Int("threshold-timer", 120, "Threshold timer defaults to 120")
	flag.Parse()
	errorIfLessThanOne(*interval, "interval")
	errorIfLessThanOne(*reqPerSec, "req-per-sec")
	errorIfLessThanOne(*thresholdTimer, "threshold-timer")

	config = ltopConfig{*source, *interval, *reqPerSec, *thresholdTimer, nil}
}

func errorIfLessThanOne(v int, name string) {
	if v <= 0 {
		fmt.Printf("Setting %s must be >= 1 it was set to %d\n", name, v)
		os.Exit(1)
	}
}

func main() {
	if logFile, err := openFile(config.source); err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("The file %s does not exist", config.source)
		}
		if os.IsPermission(err) {
			fmt.Printf("Unable to read file %s, please check the permissions and try again", config.source)
		}
		os.Exit(1)
	} else {
		_, err := logFile.Seek(0, io.SeekEnd)
		if err != nil {
			fmt.Println("Unable to attach seek to file " + err.Error())
			os.Exit(1)
		}
		config.logFile = logFile
		if err := ui.Init(); err != nil {
			createUIWithoutTty()
		} else {
			createUI()
		}
	}
}

func sleep() {
	sleepDuration := fmt.Sprintf("%ds", config.interval)
	timeIntervalDuration, err := time.ParseDuration(sleepDuration)
	if err != nil {
		fmt.Printf("Unable to parse sleep duration %s", sleepDuration)
		os.Exit(1)
	}
	time.Sleep(timeIntervalDuration)
}

func createUIWithoutTty() {
	//TODO
	fmt.Println("Currently unsupported but plan on supporting in future releases")
	os.Exit(1)
}

func formattedNowTime() string {
	return time.Now().Format(time.ANSIC)
}
