#! /bin/sh

set -e


if [ -t 0 ] ; then
    echo "(interactive shell)"
else
    echo "The app requires tty and interactive shell, please run the container with -ti"
    exit 1
fi

go run . $*

exit 0