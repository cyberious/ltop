package main

import (
	"fmt"
	"testing"

	"github.com/Songmu/axslogparser"
)

func addApacheLogEntry(le apacheLogEntries, section string, status int, count int) apacheLogEntries {
	for i := 1; i <= count; i++ {
		le = append(le, apacheLogEntry{Section: section, Log: &axslogparser.Log{User: "a", Status: status}})
	}
	return le
}

func TestCalcSectionTotals(t *testing.T) {
	le := addApacheLogEntry(apacheLogEntries{}, "/", 200, 4)

	req, total := calcSectionTotals(le)
	if req == nil {
		t.Errorf("Expected a non nul value")
	}
	if len(req) != 1 {
		t.Errorf("Expected total section rows of 1 got %d\n%s", len(req), req[1].Section)
	}
	if total != 4 {
		t.Errorf("Expected total hits of 4 but got %d", total)
	}
	le = addApacheLogEntry(le, "/report", 200, 5)
	le = addApacheLogEntry(le, "/user", 200, 10)
	req, total = calcSectionTotals(le)

	if len(req) != 3 && total != 3 {
		t.Errorf("Expected total section rows of 2 got %d", len(req))
	}
	if req[0].Section != "/user" {
		t.Errorf("Expected to have /user as top entry but got %s with value of %d", req[0].Section, req[0].totalCount)
	}
	if req[1].Section != "/report" {
		t.Error("Expected to have /report as second entry but got " + req[1].Section)
	}
	if req[0].totalCount < req[1].totalCount {
		t.Error("Expected to be sorted in descending order")
	}
}
func TestCreateSectionTotalsMap(t *testing.T) {
	var le apacheLogEntries
	for i, v := range []string{"/report", "/user"} {
		le = addApacheLogEntry(le, v, 200, 5*(i+1))
	}
	reqMap := createSectionTotalsMap(le)
	if len(reqMap) != 2 {
		t.Error("Expected to have exactly 2 entries")
	}
	if reqMap["/report"].Section != "/report" {
		t.Error("Expected key to equal section")
	}

	if reqMap["/user"].Section != "/user" {
		t.Error("Expected key to equal section")
	}
}

func TestNewApacheLogEntry(t *testing.T) {
	rootLine := "127.0.0.1 - james [09/May/2018:16:00:39 +0000] \"GET / HTTP/1.0\" 200 1234"
	reportLine := "127.0.0.1 - james [09/May/2018:16:00:39 +0000] \"GET /report HTTP/1.0\" 200 1234"
	apiLine := "127.0.0.1 - jill [09/May/2018:16:00:41 +0000] \"GET /api/user HTTP/1.0\" 200 1234"

	rootEntry := newApacheLogEntry(rootLine)
	if rootEntry.Section != "/" {
		t.Errorf("Epxect to get / but got %s\n", rootEntry.Section)
	}
	reportEntry := newApacheLogEntry(reportLine)
	if reportEntry.Section != "/report" {
		t.Errorf("Expected /report got %s original requestURI %s", reportEntry.Section, reportEntry.RequestURI)
	}

	apiEntry := newApacheLogEntry(apiLine)
	if apiEntry.Section != "/api" {
		t.Error("Expected /api")
	}
}

func TestCreateReqSectionResults(t *testing.T) {
	logStrings := []string{
		"127.0.0.1 - james [09/May/2018:16:00:39 +0000] \"GET /report HTTP/1.0\" 200 1234",
		"127.0.0.1 - jill [09/May/2018:16:00:41 +0000] \"GET /api/user HTTP/1.0\" 200 1234",
		"127.0.0.1 - jill [09/May/2018:16:00:41 +0000] \"GET /api/user HTTP/1.0\" 200 1234",
		"127.0.0.1 - james [09/May/2018:16:01:10 +0000] \"GET /report HTTP/1.0\" 200 1234",
		"127.0.0.1 - james [09/May/2018:16:01:39 +0000] \"GET /report HTTP/1.0\" 200 1234",
	}

	var logs []apacheLogEntry
	for _, line := range logStrings {
		logEntry := newApacheLogEntry(line)
		logs = append(logs, logEntry)
	}
	sr := newSectionalRequests(logs, newTimeBox(ltopConfig{interval: 10, thresholdTime: 120}))
	if len(sr.sections) != 2 {
		t.Errorf("Expected 2 entries for requests but got %d", len(sr.sections))
	}
	if sr.sections[0].Section != "/report" {
		t.Errorf("Expected '/report' to be first entry but got %s", sr.sections[0].Section)
	}
}

func TestCalcRequestsPerSecond(t *testing.T) {
	expected := float64(1)
	got := calcRequestsPerSecond(2, 2)
	if got != expected {
		t.Errorf("Expected to get %f got %f", expected, got)
	}
	expected = 10
	got = calcRequestsPerSecond(200, 20)
	if got != expected {
		t.Errorf("Expected to get %f got %f", expected, got)
	}
}

func TestThresholdMet(t *testing.T) {
	expected := false
	config := ltopConfig{"access.log", 1, 1, 10, nil}
	tb := timeBox{maxSize: 5, latestTotal: 5, allRequestTotals: []int{1, 1, 1, 1, 1}}
	msg, got := tb.thresholdMet(config)
	expectedMsg := "Threshold requests 5 of 10\n"
	if msg != expectedMsg {
		t.Errorf("Expected msg \"%s\" but got \"%s\"", expectedMsg, msg)
	}
	if expected != got {
		t.Error("Awaiting recovery should default to false")
	}

	// Lower threshold for test
	config.thresholdTime = 5
	expected = true
	msg, got = tb.thresholdMet(config)
	fmtTime := formattedNowTime()
	if expected != got {
		t.Errorf("Expect to get thresholdMet of %t but got %t", expected, got)
	}
	expectedMsg = fmt.Sprintf("Threshold requests 5 of 5\nHigh traffic generated an alert - hits = 5, triggered at %s", fmtTime)
	if msg != expectedMsg {
		t.Errorf("Expected msg \"%s\" but got \"%s\"", expectedMsg, msg)
	}
	if !tb.awaitingRecovery {
		t.Errorf("We should have set awaitingRecovery to be true but got %t", tb.awaitingRecovery)
	}

	// bump expectations to 10 per sec to ensure recovery
	expected = false
	config.reqPerSec = 10
	msg, got = tb.thresholdMet(config)
	expectedMsg = fmt.Sprintf("Threshold requests 5 of 50\nRecovered from high traffic at %s", formattedNowTime())
	if expected != got {
		t.Errorf("Expect to get thresholdMet of %t but got %t", expected, got)
	}
	if msg != expectedMsg {
		t.Errorf("Expected a total of 5 but got %s", msg)
	}
	if tb.awaitingRecovery {
		t.Errorf("We should have set awaitingRecovery to be false but got %t", tb.awaitingRecovery)
	}
}

func TestGetCurrentThroughputPercentage(t *testing.T) {
	expected := float64(100)
	got := getCurrentThroughputPercentage(100, 10, 10)
	if got != expected {
		t.Errorf("Expected to get %f got %f", expected, got)
	}
	expected = 50
	got = getCurrentThroughputPercentage(50, 10, 10)
	if got != expected {
		t.Errorf("Expected to get %f got %f", expected, got)
	}

	expected = 12
	got = getCurrentThroughputPercentage(12, 10, 10)
	if got != expected {
		t.Errorf("Expected to get %f got %f", expected, got)
	}
}
