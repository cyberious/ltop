package main

import (
	"fmt"
	"strconv"
	"time"

	ui "github.com/gizak/termui"
)

func createSparkLine(label string, twoMinData []int, color ui.Attribute) ui.Sparkline {
	s := ui.NewSparkline()
	s.Title = label
	s.Height = 7
	s.LineColor = color
	s.Data = twoMinData
	return s
}
func addSparklines(sp ...ui.Sparkline) *ui.Sparklines {
	sparkLines := ui.NewSparklines(sp...)
	sparkLines.Height = 10
	sparkLines.Width = 120
	sparkLines.Align()
	return sparkLines
}

func createUI() {
	defer ui.Close()
	createHandlers()
	instCol := buildInstructionPanel()
	configPanel := buildCurrentConfigPanel(config)
	dataset := newTimeBox(config)
	ui.Body.Align()
	draw := func(count int) {
		ui.Clear()

		ui.Body.Rows = nil // clear rows before redraw
		entries := processCurrentResults(config.logFile)
		sr := newSectionalRequests(entries, dataset)
		sections, totalCount := calcSectionTotals(entries)

		spCol := ui.NewRow(
			ui.NewCol(4, 0, addSparklines(createSparkLine("Ok Status", sr.dataset.okRequestTotals, ui.ColorGreen))),
			ui.NewCol(4, 0, addSparklines(createSparkLine("Client Errors", sr.dataset.clientErrTotals, ui.ColorMagenta))),
			ui.NewCol(4, 0, addSparklines(createSparkLine("Server Errors", sr.dataset.serverErrTotals, ui.ColorRed))),
		)
		spCol.Height = 10
		table := createSectionTable(sections, totalCount)
		trafficGauge := twoMinTrafficGauge(&sr)

		ui.Body.AddRows(
			ui.NewRow(
				instCol,
				nowTimeRow(),
			),
			configPanel,
		)

		msg, highHit := sr.dataset.thresholdMet(config)
		ui.Body.AddRows(createFlashMessageRow(msg, highHit))

		ui.Body.AddRows(
			ui.NewRow(
				ui.NewCol(12, 0, trafficGauge),
			),
			spCol,
			ui.NewRow(
				ui.NewCol(12, 0, table),
			),
		)
		ui.Body.Align()

		ui.Render(ui.Body)
	}

	drawTicker := time.NewTicker(time.Second)
	drawTickerCount := 1
	go func() {
		for {
			draw(drawTickerCount)

			drawTickerCount++
			<-drawTicker.C
			sleep()
		}
	}()
	ui.Loop()
}
func createFlashMessageRow(msg string, errMsg bool) *ui.Row {
	flashMsg := ui.NewPar(msg)
	flashMsg.Border = false
	flashMsg.Height = 2
	if errMsg {
		flashMsg.TextFgColor = ui.ColorRed
	}

	return ui.NewRow(ui.NewCol(10, 0, flashMsg))
}

func createRow(req requestSection) []string {
	return []string{
		req.Section,
		strconv.Itoa(req.okStatusCount),
		strconv.Itoa(req.clientErrCount),
		strconv.Itoa(req.serverErrCount),
		strconv.Itoa(req.totalCount),
	}
}

func createRowData(r []requestSection) [][]string {
	rowData := [][]string{{"Section", "Ok Status Count", "Client Error", "Server Error", "Total requests"}}
	for _, result := range r {
		rowData = append(rowData, createRow(result))
	}

	return rowData
}

func buildInstructionPanel() *ui.Row {
	instPanel := ui.NewPar(":PRESS q TO QUIT")
	instPanel.Height = 2
	instPanel.Width = 50
	instPanel.TextFgColor = ui.ColorWhite
	instPanel.Border = false

	return ui.NewCol(5, 0, instPanel)
}

func buildCurrentConfigPanel(c ltopConfig) *ui.Row {
	currentConfigPanel := ui.NewPar(
		fmt.Sprintf("interval: %d, threshold-timer: %d, req-per-sec: %d, source: %s", c.interval, c.thresholdTime, c.reqPerSec, c.source),
	)
	currentConfigPanel.Height = 3
	currentConfigPanel.Float = ui.AlignTop
	return ui.NewRow(ui.NewCol(12, 0, currentConfigPanel))
}

func nowTimeRow() *ui.Row {
	timePanel := ui.NewPar(formattedNowTime())
	timePanel.Border = false
	return ui.NewCol(5, 0, timePanel)
}

func createHandlers() {
	ui.Handle("/sys/kbd/q", func(ui.Event) {
		// press q to quit
		ui.ResetHandlers()
		ui.StopLoop()
	})
	// handle a 1s timer
	ui.Handle(fmt.Sprintf("/timer/%ds", config.interval), func(e ui.Event) {
		t := e.Data.(ui.EvtTimer)
		// t is a EvtTimer
		if t.Count%2 == 0 {
			ui.Render(ui.Body)
		}
	})
}

func twoMinTrafficGauge(sr *requests) *ui.Gauge {
	g := ui.NewGauge()
	g.Height = 3
	g.Y = 11
	pct := int(getCurrentThroughputPercentage(sr.dataset.latestTotal, config.reqPerSec, config.interval))
	g.Percent = pct
	g.BorderLabel = fmt.Sprintf("Maximum request per second of %d/%d with a maximum of %d during the interval of %d", sr.dataset.latestTotal/config.interval, config.reqPerSec, sr.dataset.maxSize, config.interval)
	var color ui.Attribute
	switch {
	case pct >= 75:
		color = ui.ColorRed
	case pct >= 50:
		color = ui.ColorYellow
	default:
		color = ui.ColorCyan
	}
	g.BorderLabel = "Gauge"
	g.BarColor = color
	g.BorderFg = ui.ColorWhite
	g.BorderLabelFg = ui.ColorCyan

	return g
}
func createSectionTable(sections []requestSection, totalItems int) *ui.Table {
	table := ui.NewTable()
	expectedRows := len(sections)
	table.BorderLabel = fmt.Sprintf("Total Requests %d", totalItems)
	table.Rows = createRowData(sections)
	table.FgColor = ui.ColorDefault
	table.BgColor = ui.ColorDefault
	rowMultiplier := 2.0
	//table.CellWidth = []int{20, 20, 20, 20, 20}
	table.TextAlign = ui.AlignCenter
	table.Analysis()
	table.SetSize()
	table.Height = int(rowMultiplier * float64(expectedRows+2))

	return table
}
